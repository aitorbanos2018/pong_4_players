﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Arkanoid : MonoBehaviour
{
    public float velocidadX;
    public float maxX;
    public float minX;
    private float posX;
    private float direction;

    // Update is called once per frame
    void Update()
    {

        direction = Input.GetAxis("Horizontal");

        posX = transform.position.x + direction*velocidadX*Time.deltaTime;

        if(posX>maxX){
            posX = maxX;
        }

        if (posX < minX)
        { posX = minX; }

        transform.position = new Vector3(posX, transform.position.y, transform.position.z);       
    }
}
